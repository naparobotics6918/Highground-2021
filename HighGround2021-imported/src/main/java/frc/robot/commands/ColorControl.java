/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

/*package frc.robot.commands;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandBase;
import edu.wpi.first.wpilibj.DriverStation;
import frc.robot.subsystems.ControlPanel;
import edu.wpi.first.wpilibj.util.Color;
import com.revrobotics.ColorMatchResult;
import edu.wpi.first.wpilibj.Joystick;
import com.revrobotics.ColorSensorV3;
import com.revrobotics.ColorMatch;
import edu.wpi.first.wpilibj.I2C;

public class ColorControl extends CommandBase {
  private final I2C.Port i2cPort = I2C.Port.kOnboard;
  private final Color kYellowTarget = ColorMatch.makeColor(0.361, 0.524, 0.113);
  private final Color kGreenTarget = ColorMatch.makeColor(0.197, 0.561, 0.240);
  private final Color kBlueTarget = ColorMatch.makeColor(0.143, 0.427, 0.429);
  private final Color kRedTarget = ColorMatch.makeColor(0.561, 0.232, 0.114);
  private final ColorSensorV3 m_colorSensor = new ColorSensorV3(i2cPort);
  private final ColorMatch m_colorMatcher = new ColorMatch();
  private final ControlPanel controlpanel;
  Joystick controlstick = new Joystick(1);
  Joystick drivestick = new Joystick(0);
  private String desiredcolor;
  private String colorString;
  private int desiredcolorN;
  private int colorStringN;
  private double stuck;

  public ColorControl(ControlPanel m_controlpanel) {
    controlpanel = m_controlpanel;
    addRequirements(controlpanel);
  }

  @Override
  public void initialize() {
    m_colorMatcher.addColorMatch(kBlueTarget);
    m_colorMatcher.addColorMatch(kGreenTarget);
    m_colorMatcher.addColorMatch(kRedTarget);
    m_colorMatcher.addColorMatch(kYellowTarget);
  }
  
  @Override
  public void execute() {
    Color detectedColor = m_colorSensor.getColor();
    ColorMatchResult match = m_colorMatcher.matchClosestColor(detectedColor);

    //Translates desired color into a number
    desiredcolor = DriverStation.getInstance().getGameSpecificMessage();
    if(desiredcolor.charAt(0) == 'B') {
      desiredcolorN = 0;
    } else if(desiredcolor.charAt(0) == 'G') {
      desiredcolorN = 1;
    } else if(desiredcolor.charAt(0) == 'R') {
      desiredcolorN = 2;
    } else if(desiredcolor.charAt(0) == 'Y') {
      desiredcolorN = 3;
    }

    //Puts detected color into a string variable
    if(match.color == kBlueTarget) {
      colorString = "Blue";
    } else if(match.color == kRedTarget) {
      colorString = "Red";
    } else if(match.color == kGreenTarget) {
      colorString = "Green";
    } else if(match.color == kYellowTarget) {
      colorString = "Yellow";
    } else {
      colorString = "Unknown";
    }

    //translates detected color into a number + 2
    if(colorString.charAt(0) == 'B') {
      colorStringN = 2;
    } else if(colorString.charAt(0) == 'G') {
      colorStringN = 3;
    } else if(colorString.charAt(0) == 'R') {
      colorStringN = 0;
    } else if(colorString.charAt(0) == 'Y') {
      colorStringN = 1;
    }

    //determines what to do based on the differences between the desired and detected colors
    if(colorStringN == (desiredcolorN-1 % 4)) {
      ControlPanel.spinner(0.15+(stuck/1000));
      stuck=stuck+1;
    } else if(colorStringN == (desiredcolorN+1 % 4)) {
      ControlPanel.spinner(-0.15-(stuck/1000));
      stuck=stuck+1;
    } else if(colorStringN != (desiredcolorN+0 % 4)) {
      ControlPanel.spinner(0.25);
      stuck=0;
    } else {
      ControlPanel.spinner(ControlPanel.spinnerPos());
      stuck=0;
    }

    //Puts variables into SmartDashboard
    SmartDashboard.putString("Detected Color", colorString);
    SmartDashboard.putString("Desired Color", desiredcolor);
  }

  @Override
  public void end(boolean interrupted) {
  }

  @Override
  public boolean isFinished() {
    return false;
  }
}*/