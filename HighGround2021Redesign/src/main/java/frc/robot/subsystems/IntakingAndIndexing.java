// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.can.VictorSPX;

import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;
import frc.robot.beamBreak;

public class IntakingAndIndexing extends SubsystemBase {
  static VictorSPX indexingV = new VictorSPX(Constants.indexingVID);
  static VictorSPX intake = new VictorSPX(Constants.intakeID);
  static VictorSPX Conveyer = new VictorSPX(Constants.coveyerID);
  static DoubleSolenoid intakeSolenoid = new DoubleSolenoid(Constants.intakeSolenoid[0], Constants.intakeSolenoid[1]);
  static beamBreak s_bbAlpha = new beamBreak(Constants.alphabbsensor);
  static beamBreak s_bbBeta = new beamBreak(Constants.betabbsensor);
  static beamBreak s_bbGamma = new beamBreak(Constants.gammabbsensor);
  static public int ballCount = 0;
  static public boolean newBallEvent = false;
  static public boolean ballsSolved = true;

  public IntakingAndIndexing() {
    Conveyer.setInverted(true);
    indexingV.setInverted(true);
    intake.setInverted(false);
  }

  @Override
  public void periodic() {

    if(s_bbAlpha.get() && s_bbBeta.get()){
      if(s_bbBeta.risingEdge() && Conveyer.getMotorOutputPercent() > 0){
        incrementBallCount();
      }
      else if(Conveyer.getMotorOutputPercent() < 0 && s_bbAlpha.risingEdge()){
        decrementBallCount();
      }
    }

    if(s_bbGamma.risingEdge()){
      decrementBallCount();
    }

    SmartDashboard.putNumber("Ball Count", ballCount);
  }

  void incrementBallCount(){
    ballCount++;
    newBallEvent = true;
  }
  void decrementBallCount(){
    ballCount--;
  }

  public void  setIntake(double intakedemand, double indexingVDemand, double conveyerDemand) {
    indexingV.set(ControlMode.PercentOutput, indexingVDemand);
    intake.set(ControlMode.PercentOutput, intakedemand);
    Conveyer.set(ControlMode.PercentOutput, conveyerDemand);
}

  public void setIntake(double demand) {
    intake.set(ControlMode.PercentOutput, demand);
  }

  public void setConveyer(double demand) {
    Conveyer.set(ControlMode.PercentOutput, demand);;
  }

  public void setIndexingV(double demand) {
    indexingV.set(ControlMode.PercentOutput, demand);;
  }

  /*public void setIntakeSolenoid(DoubleSolenoid.Value value) {
    intakeSolenoid.set(value);
  }*/

  public static boolean getbbAlpha(){
    return s_bbAlpha.get();
  }

  public static boolean getbbBeta(){
    return s_bbBeta.get();
  }

  @Override
  public void simulationPeriodic() {
    // This method will be called once per scheduler run during simulation
  }
}
