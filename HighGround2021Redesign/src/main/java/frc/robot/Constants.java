// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

/**
 * The Constants class provides a convenient place for teams to hold robot-wide numerical or boolean
 * constants. This class should not be used for any other purpose. All constants should be declared
 * globally (i.e. public static). Do not put anything functional in this class.
 *
 * <p>It is advised to statically import this class (or one of its inner classes) wherever the
 * constants are needed, to reduce verbosity.
 */
public final class Constants {

    //Drivetrain system
    public static final int leftmotor1ID = 0;
    public static final int leftmotor2ID = 1;
    public static final int rightmotor1ID = 2;
	public static final int rightmotor2ID = 3;

	//Intake system
	public static final int indexingVID = 7;
	public static final int intakeID = 0;
	public static final int coveyerID = 4;
	public static final int[] intakeSolenoid = {0,1};
	public static final int alphabbsensor = 5;
	public static final int betabbsensor = 4;
	public static final int gammabbsensor = 3;

	//Climb system
    public static final int lin1ID = 11;
    public static final int lin2ID = 12;
	public static final int linH1ID = 10;
	public static final int linH2ID = 7;

	//Shooter system
	public static final int shooterlID = 4;
	public static final int shooterrID = 5;

	//Drivestick Axes
	public static final int verticalID = 1;
	
	//Drivestick Buttons
	public static final int AutoToggleID = 1;
	

	public static final class velocitycontrol{
			/**
		 * Which PID slot to pull gains from. Starting 2018, you can choose from
		 * 0,1,2 or 3. Only the first two (0,1) are visible in web-based
		 * configuration.
		 */
		public static final int kSlotIdx = 0;

		/**
		 * Talon FX supports multiple (cascaded) PID loops. For
		 * now we just want the primary one.
		 */
		public static final int kPIDLoopIdx = 0;

		/**
		 * Set to zero to skip waiting for confirmation, set to nonzero to wait and
		 * report to DS if action fails.
		 */
		public static final int kTimeoutMs = 30;

		/**
		 * PID Gains may have to be adjusted based on the responsiveness of control loop.
		 * kF: 1023 represents output value to Talon at 100%, 20660 represents Velocity units at 100% output
		 * 
		 * 	                                    			  kP  kI    kD      kF          Iz    PeakOut */
	public final static Gains kGains_Velocit  = new Gains( 0.08, 0.0000001, 5, 1023.0/20660.0,  300,  1.00);
		}
}
