// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import edu.wpi.first.wpilibj2.command.SubsystemBase;
import edu.wpi.first.wpilibj.Joystick;

public class GlobalVaribles extends SubsystemBase {
  Joystick drivestick = new Joystick(0);
  public static double drivestickYAxis;
  public static double drivestickZAxis;
  public static boolean intaking = false;

  /** Creates a new ExampleSubsystem. */
  public GlobalVaribles() {
  }

  @Override
  public void periodic() {
    // This method will be called once per scheduler run
    drivestickYAxis = drivestick.getY();
    drivestickZAxis = drivestick.getZ();
  }

  public static double getDrivestickYAxis() {
    return drivestickYAxis;
  }

  public static double getDrivestickZAxis() {
    return drivestickZAxis;
  }

  public static boolean getIntaking(){
    return intaking;
  }
  public static void setIntaking(boolean setIntaking) {
    intaking = setIntaking;
  }

  @Override
  public void simulationPeriodic() {
    // This method will be called once per scheduler run during simulation
  }
}
