package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import edu.wpi.first.wpilibj2.command.ParallelRaceGroup;
import frc.robot.subsystems.BallManipulation;
import frc.robot.subsystems.ShooterControl;
import frc.robot.subsystems.Drivetrain;
import frc.robot.Constants;

public class Auto extends SequentialCommandGroup {
  /**
   * Creates a new ComplexAuto.
   *
   * @param drive The drive subsystem this command will run on
   * @param hatch The hatch subsystem this command will run on
   */
  public Auto(Drivetrain drivetrain, ShooterControl shootercontrol, BallManipulation manip) {
    addCommands(
        
        // Drive backward the specified distance
        new Drivedistance(Constants.autoconstants.drivedistance, -Constants.autoconstants.drivespeed,
        drivetrain),///*
        
        // auto align
        new AutoPilot(drivetrain),

        // Rev up the shooter
        new Shooter(shootercontrol, true),

        // Release the power cells into the ports
        new ParallelRaceGroup(new Shooter(shootercontrol, false), new Intake(manip)));
  }

}