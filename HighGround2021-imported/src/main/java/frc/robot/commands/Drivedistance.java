package frc.robot.commands;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Constants;
import frc.robot.PID;
import frc.robot.subsystems.Drivetrain;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.shuffleboard.Shuffleboard;

import java.lang.Math;

public class Drivedistance extends CommandBase {
  private final Drivetrain m_Drivetrain;
  double rotation, vertical;
  double distanceinch;
  PID verticalcontrol = new PID(0.1, 0.25, 0, 0.8765432101234567890987654321234567890987654321);
  PID horizantolcontrol = new PID(0.02, 0, 0, 0.4765432101234567890987654321234567890987654321);
  double gearing = 14.88;
  double wheelRadius = 6*Constants.Pi;
  double positionsPerTurn = 2098;
  boolean finished = false;

  public Drivedistance(double m_distance, double speed, Drivetrain system) {
    m_Drivetrain = system;
    addRequirements(m_Drivetrain);
    distanceinch = m_distance;
    //verticalcontrol.settarget((((distanceinch/positionsPerTurn)/gearing)*wheelRadius));
    verticalcontrol.settarget(distanceinch);
    horizantolcontrol.settarget(0);
  }
  
  @Override
  public void initialize() {
    NetworkTableInstance.getDefault().getTable("limelight").getEntry("ledMode").setNumber(0);
    Drivetrain.resetdriveencoders();
    verticalcontrol.settarget(distanceinch);
  }

  @Override
  public void execute() {
    double distancedriven = (((Drivetrain.getdistanceave()/positionsPerTurn)/gearing)*wheelRadius);
    double distancediffernece = ((Drivetrain.getdistancedifference()/positionsPerTurn)/gearing)*wheelRadius;
    vertical = -(verticalcontrol.pidcontrol(distancedriven));
    rotation = -(horizantolcontrol.pidcontrol(distancediffernece));
    SmartDashboard.putNumber("Autonomous Vertical", vertical);
    SmartDashboard.putNumber("Autonomous Rotation", rotation);
    SmartDashboard.putNumber("Distance Driven", distancedriven);
    SmartDashboard.putNumber("Distance Defference", distancediffernece);
    SmartDashboard.putNumber("Left Distance", Drivetrain.getleftmotordistance());
    SmartDashboard.putNumber("Right Distance", Drivetrain.getrightmotordistance());
    SmartDashboard.putNumber("Drive Error", verticalcontrol.geterror());

    Drivetrain.RKDrive(vertical, rotation, 0.75);

    if((Math.abs(verticalcontrol.geterror()) < 1.0) && Math.abs(horizantolcontrol.geterror()) < 5.0){
      finished = true;
    }
  }

  @Override
  public void end(boolean interrupted) {
  }

  @Override
  public boolean isFinished() {
    return finished;
  }
}
