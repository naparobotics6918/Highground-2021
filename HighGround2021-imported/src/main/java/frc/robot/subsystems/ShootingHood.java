/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import edu.wpi.first.networktables.NetworkTableInstance;
import com.ctre.phoenix.motorcontrol.can.WPI_VictorSPX;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.FeedbackDevice;
import com.ctre.phoenix.motorcontrol.can.TalonSRX;
import edu.wpi.first.wpilibj.DigitalInput;
import frc.robot.Constants;

public class ShootingHood extends SubsystemBase {
  static WPI_TalonSRX shooterr = new WPI_TalonSRX(Constants.shooterrID);
  static WPI_VictorSPX shooterl = new WPI_VictorSPX(Constants.shooterlID);
  static TalonSRX ShootingHood = new TalonSRX(Constants.ShootingHoodID);
  DigitalInput LimitSwitchTest = new DigitalInput(9);
  double tx;

  public ShootingHood() {
    ShootingHood.setSensorPhase(false);
    ShootingHood.setInverted(true);
  }

  @Override
  public void periodic() {
  }

  public static void ShootingHood(double value) {
    ShootingHood.set(ControlMode.PercentOutput, value);
  }

  public static void resetHood() {
    ShootingHood.setSelectedSensorPosition(0);
  }

  public static double hoodPos() {
    return ShootingHood.getSelectedSensorPosition();
  }

}
