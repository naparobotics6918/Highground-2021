/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

import edu.wpi.first.wpilibj2.command.button.JoystickButton;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj.Joystick;
import frc.robot.subsystems.BallManipulation;
import frc.robot.subsystems.ShooterControl;
//mport frc.robot.subsystems.ControlPanel;
import frc.robot.subsystems.Drivetrain;
//import frc.robot.subsystems.ClimbBot;
//import frc.robot.commands.ColorRecognition;
//import frc.robot.commands.LinearActuators;
//import frc.robot.commands.RotationControl;
//import frc.robot.commands.ColorControl;
import frc.robot.commands.AutoPilot;
import frc.robot.commands.TankDrive;
import frc.robot.commands.Intake;

public class RobotContainer {
  //Controllers
  Joystick controlstick = new Joystick(1);
  Joystick drivestick = new Joystick(0);

  //Control Panel [Deactivated]
  //private final ControlPanel s_controlPanel = new ControlPanel();
  //private final ColorRecognition c_colorRecognition = new ColorRecognition(s_controlPanel);
  //private final RotationControl c_rotationControl = new RotationControl(s_controlPanel);
  //private final ColorControl c_colorControl = new ColorControl(s_controlPanel);
  //Drivetrain
  private final Drivetrain s_driveTrain = new Drivetrain();
  private final TankDrive c_tankDrive = new TankDrive(s_driveTrain);
  private final AutoPilot c_autoPilot = new AutoPilot(s_driveTrain);
  //Intake and Ball Conveyer
  private final BallManipulation s_ballmanipulation = new BallManipulation();
  private final Intake c_intake = new Intake(s_ballmanipulation);
  //Turret
  private final ShooterControl s_shooterControl = new ShooterControl();
  //private final Shooter c_shooter = new Shooter(s_shooterControl, false);
  //Climbing
  //private final ClimbBot s_climbBot = new ClimbBot();
  //private final LinearActuators c_linearActuators = new LinearActuators(s_climbBot);
  
  //Subsystems, OI devices, and commands.
  public RobotContainer() {
    configureButtonBindings();
    //s_controlPanel.setDefaultCommand(c_colorRecognition);
    //s_climbBot.setDefaultCommand(c_linearActuators);
    s_ballmanipulation.setDefaultCommand(c_intake);
    //s_shooterControl.setDefaultCommand(c_shooter);
    s_driveTrain.setDefaultCommand(c_tankDrive);
  }

  private void configureButtonBindings() {
    //new JoystickButton(controlstick, Constants.rotationControlID).whenPressed(c_rotationControl);
    //new JoystickButton(controlstick, Constants.colorControlID).whenPressed(c_colorControl);
    new JoystickButton(drivestick, Constants.AutoToggleID).whenPressed(c_autoPilot);
  }

  public Command getAutonomousCommand() {
    return null;//new Auto(/*s_driveTrain, s_shooterControl, s_ballmanipulation*/);
  }
}
