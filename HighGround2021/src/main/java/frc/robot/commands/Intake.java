package frc.robot.commands;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandBase;
import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import frc.robot.subsystems.BallManipulation;
import edu.wpi.first.wpilibj.Joystick;
import frc.robot.commands.Shooter;
import frc.robot.Constants;
import frc.robot.Robot;


public class Intake extends CommandBase{
    BallManipulation m_ballmanipulation;
    boolean balldetected, balldetected2, balldetected3, balldetectedfinal, intakeextened = false, intaking = false;
    double conveyerspeed, InputVertical, InputHorizontal, seconds = 0;
    boolean autostarted = false, gointake = false;
    Joystick controlstick = new Joystick(1);
    Joystick drivestick = new Joystick(0);
    int inputStage = 1;


    public Intake(BallManipulation ballmanipulation) {
        m_ballmanipulation = ballmanipulation;
        addRequirements(m_ballmanipulation);
    }

    @Override
    public void initialize() {
        autostarted = false;
        inputStage = 1;
        seconds = 0;
    }

    @Override
    public void execute() {
        //Autonomous period
        if(Robot.autonomous) {
            if(!autostarted) {
                gointake = true;
                autostarted = true;
            }
        }

        //Intake Extender Logic
        if(controlstick.getRawButtonPressed(Constants.IntakeExtenderID)) {
            gointake = true;
        }
        if(gointake) {
            gointake = false;
            intakeextened = !intakeextened;
            if(intakeextened) {
                m_ballmanipulation.setintakesoleboi(Value.kForward);
            } else {
                m_ballmanipulation.setintakesoleboi(Value.kReverse);
            }
        } else {
            m_ballmanipulation.setintakesoleboi(Value.kOff);
        }

        //Prevents momentary obstruction from being picked up by the beam break sensor
        balldetected3 = balldetected2;
        balldetected2 = balldetected;
        balldetected = m_ballmanipulation.getbbsensor();
        if(balldetected == true && balldetected2 == true && balldetected3 == true) { 
            balldetectedfinal = true;
        } else if(balldetected == false && balldetected2 == false && balldetected3 == false) {
            balldetectedfinal = false;
        }

        //toggle intaking on or off
        if(controlstick.getRawButtonPressed(Constants.ToggleIntakeID)) {
            intaking = !intaking;
            inputStage = 1;
        }

        //intake sequence logic
        if(intaking == true) {
            switch(inputStage) {
                case(1): //Default Intaking State
                conveyerspeed = 0;
                InputHorizontal = 1;
                InputVertical = -1;
                if(balldetectedfinal == true) {
                    inputStage = 2;
                }
                break;
                case(2): //Move the conveyer and stop the intake once the ball is detected
                conveyerspeed = 0.385;
                InputHorizontal = 0;
                InputVertical = 0;
                if(balldetectedfinal == false) {
                    inputStage = 1;
                }
                break;
                case(3): //Keep moving the conveyer momentarily after ball is not detected
                seconds += 0.02;
                if(seconds > 0.1) {
                    inputStage = 1;
                }
                else if (seconds < 0.1){
                    conveyerspeed = 0.385;
                }
                break;
            }
        } else {
            InputHorizontal = 0;
            InputVertical = 0;
            conveyerspeed = 0;
        }
        SmartDashboard.putNumber("stage", inputStage);

        //Overrides Autonomous intake logic with human inputs if the shooter is up to speed
        if(controlstick.getRawButton(Constants.ConveyerUpID) && ((Shooter.shootingvelocity > 29750 && Shooter.shootingvelocity < 31000) || Shooter.shootingvelocity < 1750)) {
            conveyerspeed = 0.5;
        } else if(controlstick.getRawButton(Constants.ConveyerDownID)) {
            conveyerspeed = -0.5;
            InputHorizontal = -1;
            InputVertical = 1;
        }
        if(Robot.autonomous) {
            conveyerspeed = 0.5;
        }
        BallManipulation.setIntake(InputVertical, InputHorizontal, conveyerspeed);
        SmartDashboard.putBoolean("BBSensor", balldetected);
    }

    @Override
    public void end(boolean interrupted){}

    @Override
    public boolean isFinished(){
        return false;
    }

}