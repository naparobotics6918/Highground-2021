// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import frc.robot.subsystems.IntakingAndIndexing;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandBase;

/** An example command that uses an example subsystem. */
public class IntakeandIndex extends CommandBase {
  @SuppressWarnings({"PMD.UnusedPrivateField", "PMD.SingularField"})
  private int stage = 0;
  private final IntakingAndIndexing m_subsystem;

  /**
   * Creates a new ExampleCommand.
   *
   * @param subsystem The subsystem used by this command.
   */
  public IntakeandIndex(IntakingAndIndexing subsystem) {
    m_subsystem = subsystem;
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(subsystem);
    stage = 0;
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {}

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    switch (stage) {
      case 0:
      
      m_subsystem.setIntake(1, 1, 0);
      if (IntakingAndIndexing.getbbAlpha()) {
        stage++;
      }

        break;
      case 1:
      m_subsystem.setIntake(1, 1, 0.4);
      if (!IntakingAndIndexing.getbbAlpha() || !IntakingAndIndexing.getbbBeta()) {
        stage++;
      }
      break;

      case 2:
      m_subsystem.setIntake(1, 1, 0.3);
      if (!IntakingAndIndexing.getbbBeta()) {
        stage++;
      }
      break;
    
      default:
      stage = 0;
        break;
    }

    SmartDashboard.putNumber("Stage", stage);
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    m_subsystem.setIntake(0, 0, 0);
    stage = 0;
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
