// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.InvertType;
import com.ctre.phoenix.motorcontrol.can.TalonFX;
import java.lang.Math;

import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;

public class Drivetrain extends SubsystemBase {
  static TalonFX m_leftMaster = new TalonFX(Constants.leftmotor1ID);
  static TalonFX m_leftFollower = new TalonFX(Constants.leftmotor2ID);
  static TalonFX m_rightMaster = new TalonFX(Constants.rightmotor1ID);
  static TalonFX m_rightFollower = new TalonFX(Constants.rightmotor2ID);

  /** Creates a new ExampleSubsystem. */
  public Drivetrain() {
    m_leftMaster.configFactoryDefault();
    m_rightMaster.configFactoryDefault();
    m_leftMaster.configNeutralDeadband(0.1);
    m_rightMaster.configNeutralDeadband(0.1);


    m_leftMaster.setInverted(InvertType.InvertMotorOutput);
    m_leftFollower.follow(m_leftMaster);
    m_leftFollower.setInverted(InvertType.FollowMaster);

    m_rightMaster.setInverted(InvertType.None);
    m_rightFollower.follow(m_rightMaster);
    m_rightFollower.setInverted(InvertType.FollowMaster);
  }

  @Override
  public void periodic() {
    // This method will be called once per scheduler run
  }

  public void setMotors(double leftMotor, double rightMotor){
    m_leftMaster.set((ControlMode.PercentOutput),leftMotor);
    m_rightMaster.set((ControlMode.PercentOutput), rightMotor);
  }
 
  @Override
  public void simulationPeriodic() {
    // This method will be called once per scheduler run during simulation
  }
}
