/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import java.util.function.BooleanSupplier;
import frc.robot.subsystems.ControlPanel;
import edu.wpi.first.wpilibj.util.Color;
import com.revrobotics.ColorMatchResult;
import edu.wpi.first.wpilibj.Joystick;
import com.revrobotics.ColorSensorV3;
import com.revrobotics.ColorMatch;
import edu.wpi.first.wpilibj.I2C;

public class RotationControl extends CommandBase {
  private final ControlPanel controlpanel;
  private final I2C.Port i2cPort = I2C.Port.kOnboard;
  private final Color kYellowTarget = ColorMatch.makeColor(0.361, 0.524, 0.113);
  private final Color kGreenTarget = ColorMatch.makeColor(0.197, 0.561, 0.240);
  private final Color kBlueTarget = ColorMatch.makeColor(0.143, 0.427, 0.429);
  private final Color kRedTarget = ColorMatch.makeColor(0.561, 0.232, 0.114);
  private final ColorSensorV3 m_colorSensor = new ColorSensorV3(i2cPort);
  private final ColorMatch m_colorMatcher = new ColorMatch();
  String color1, color2, colorstring2, colorstring, stage;
  public static BooleanSupplier ended = () -> false;
  Joystick controlstick = new Joystick(1);
  Joystick drivestick = new Joystick(0);
  boolean end = false, test = false;
  double colors, spins, spinmotor;
  int a;

  public RotationControl(ControlPanel m_controlpanel) {
    controlpanel = m_controlpanel;
    addRequirements(controlpanel);
  }

  @Override
  public void initialize() {
    colorstring = "Unknown";
    colorstring2 = colorstring;
    m_colorMatcher.addColorMatch(kRedTarget);
    m_colorMatcher.addColorMatch(kBlueTarget);
    m_colorMatcher.addColorMatch(kGreenTarget);
    m_colorMatcher.addColorMatch(kYellowTarget);
    SmartDashboard.putBoolean("RotationControl initialize", true);
  }

  @Override
  public void execute() {
    SmartDashboard.putNumber("Spins", spins);
    SmartDashboard.putString("Detected Color", colorstring);
    SmartDashboard.putBoolean("color identification", true);
    SmartDashboard.putBoolean("Rotation Control Execute", true);
    SpinCounter();
    SpinControl();
  }

  @Override
  public void end(boolean interrupted) {
    ended.equals(true);
    end = true;
  }

  @Override
  public boolean isFinished() {
    return end;
  }

  public double SpinCounter() {
    Color detectedColor = m_colorSensor.getColor();
    ColorMatchResult match = m_colorMatcher.matchClosestColor(detectedColor);

    //Translates color sensor output into a string
    if (match.color == kBlueTarget) {
      colorstring = "Blue";
    } else if (match.color == kRedTarget) {
      colorstring = "Red";
    } else if (match.color == kGreenTarget) {
      colorstring = "Green";
    } else if (match.color == kYellowTarget) {
      colorstring = "Yellow";
    } else {
      colorstring = "Unknown";
    }

    //If color has changed add one to color and calculate spins
    if(colorstring != colorstring2){
      color2 = color1;
      color1 = colorstring;

      //If color went backwards dont add to color count
      if(color2 != color1){
        colors = colors + 1;
      }
      spins = (colors / 8);
    }
    colorstring2 = colorstring;
    return spins;
  }
  
  public void SpinControl() {
    if(spins >= 3.5) {
      ControlPanel.spinner(0);
      end(true);
    } else {
      ControlPanel.spinner(0.5);
    }
  }
}