// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.IntakingAndIndexing;

public class FireConveyer extends CommandBase {
  IntakingAndIndexing iAi;
  /** Creates a new FireConveyer. */
  public FireConveyer(IntakingAndIndexing subsystem) {
    iAi = subsystem;
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(subsystem);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {}

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    iAi.setConveyer(0.5);
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    // yes ik this is equal to !(iAI.ballCount > 0);
    // but to the average observer it's easier to read this way so get off my back
    IntakingAndIndexing.ballsSolved = IntakingAndIndexing.ballCount > 0 ? false : true;
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
