// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.Shooter;

public class DistVeloShoot extends CommandBase {
  private final Shooter subsystem;
  double ty, distance;
  /** Creates a new DistVeloShoot. */
  public DistVeloShoot(Shooter decShooter) {
    subsystem = decShooter;
    addRequirements(decShooter);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    NetworkTableInstance.getDefault().getTable("limelight").getEntry("ledMode").setNumber(0);
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    ty = NetworkTableInstance.getDefault().getTable("limelight").getEntry("tx").getDouble(0) - 4.25;
    distance = 1.5736 / Math.tan(Math.toRadians(ty+22.0));

    SmartDashboard.putNumber("Distance", distance);

    double rpm = distance > 2.286 ? (distance*231.0) + 3407.4 : 5000;

    subsystem.setRPM(rpm);
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    subsystem.stop();
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
