// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import edu.wpi.first.wpilibj.DigitalInput;

/** Add your docs here. */
public class beamBreak extends DigitalInput{
    int bbint = 0, ibbint = 0;

    public beamBreak(int channel) {
        super(channel);
        // TODO Auto-generated constructor stub
    }

    public boolean risingEdge() {
        ibbint = bbint;
        bbint = getAsInt();
        return bbint > ibbint;
    }

    int getAsInt(){
        return super.get() ? 1 : 0;
    }
}
