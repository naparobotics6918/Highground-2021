package frc.robot.subsystems;

import edu.wpi.first.wpilibj2.command.SubsystemBase;

import com.ctre.phoenix.motorcontrol.can.TalonFX;
import com.ctre.phoenix.motorcontrol.can.VictorSPX;
import com.ctre.phoenix.motorcontrol.ControlMode;
import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj.DigitalInput;
import frc.robot.Constants;

public class BallManipulation extends SubsystemBase {
    static VictorSPX InputHorizontal = new VictorSPX(Constants.intakeHorizontalID);
    static VictorSPX InputVertical = new VictorSPX(Constants.intakeVerticalID);
    static VictorSPX Conveyer = new VictorSPX(Constants.coveyerID);
    DoubleSolenoid intakesoleboi = new DoubleSolenoid(0, 1);
    DigitalInput bbsensor = new DigitalInput(5);
    
    public BallManipulation(){
        Conveyer.setInverted(true);
        InputHorizontal.setInverted(true);
        InputVertical.setInverted(true);
    }

    @Override
    public void periodic() {
    }

    public static void  setIntake(double inputvertical, double inputhorizontal, double conveyerspeed) {
        InputHorizontal.set(ControlMode.PercentOutput, inputhorizontal);
        InputVertical.set(ControlMode.PercentOutput, inputvertical);
        Conveyer.set(ControlMode.PercentOutput, conveyerspeed);
    }


    public void setintakesoleboi(DoubleSolenoid.Value value){
        intakesoleboi.set(value);
    }
    
    public boolean getbbsensor(){
        return bbsensor.get();
    }
}