/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import edu.wpi.first.networktables.NetworkTableInstance;
import com.ctre.phoenix.motorcontrol.can.WPI_VictorSPX;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.FeedbackDevice;
import com.ctre.phoenix.motorcontrol.can.TalonSRX;
import edu.wpi.first.wpilibj.DigitalInput;
import frc.robot.Constants;

public class ShooterControl extends SubsystemBase {
  static WPI_TalonSRX shooterr = new WPI_TalonSRX(Constants.shooterrID);
  static WPI_VictorSPX shooterl = new WPI_VictorSPX(Constants.shooterlID);
  static TalonSRX ShootingHood = new TalonSRX(Constants.ShootingHoodID);
  DigitalInput LimitSwitchTest = new DigitalInput(9);
  double tx;

  public ShooterControl() {
    // Factory Default all hardware to prevent unexpected behaviour
    shooterr.configFactoryDefault();
    shooterl.configFactoryDefault();
    //Phase sensor accordingly. Positive Sensor Reading should match Green (blinking) Leds on Talon
    shooterr.setSensorPhase(false);
    //ShootingHood.setSensorPhase(true);
    shooterr.configClosedloopRamp(0);
    //ShootingHood.setFeedbackDevice(FeedbackDevice.QuadEncoder);
		// Config sensor used for Primary PID [Velocity]
    shooterr.configSelectedFeedbackSensor(FeedbackDevice.CTRE_MagEncoder_Relative, Constants.kPIDLoopIdx, Constants.kTimeoutMs);
		// Config the peak and nominal outputs
		shooterr.configNominalOutputForward(0, Constants.kTimeoutMs);
		shooterr.configNominalOutputReverse(0, Constants.kTimeoutMs);
		shooterr.configPeakOutputForward(1.0, Constants.kTimeoutMs);
		shooterr.configPeakOutputReverse(0.0, Constants.kTimeoutMs);
		// Config the Velocity closed loop gains in slot0 
		shooterr.config_kF(Constants.kPIDLoopIdx, Constants.kF, Constants.kTimeoutMs);
		shooterr.config_kP(Constants.kPIDLoopIdx, Constants.kP, Constants.kTimeoutMs);
		shooterr.config_kI(Constants.kPIDLoopIdx, Constants.kI, Constants.kTimeoutMs);
    shooterr.config_kD(Constants.kPIDLoopIdx, Constants.kD, Constants.kTimeoutMs);
    shooterl.follow(shooterr);
    shooterl.setInverted(false);
    shooterr.setInverted(true);
    ShootingHood.configFactoryDefault();
    ShootingHood.configSelectedFeedbackSensor(FeedbackDevice.QuadEncoder);
    ShootingHood.setSensorPhase(false);
    ShootingHood.setInverted(true);
  }

  @Override
  public void periodic() {
    tx = NetworkTableInstance.getDefault().getTable("limelight").getEntry("tx").getDouble(1);
    SmartDashboard.putNumber("Right Velocity", shooterr.getSelectedSensorVelocity());
    SmartDashboard.putNumber("Right Motor Speed", shooterr.getMotorOutputPercent());
    SmartDashboard.putNumber("Left Velocity", shooterl.getSelectedSensorVelocity());
    SmartDashboard.putNumber("Left Motor Speed", shooterl.getMotorOutputPercent());
    SmartDashboard.putNumber("Right Motor Temp", shooterr.getTemperature());
    SmartDashboard.putNumber("Left Motor Temp", shooterl.getTemperature());
  }

  public static void ShootingHood(double value) {
    ShootingHood.set(ControlMode.PercentOutput, value);
  }

  public static void resetHood() {
    ShootingHood.setSelectedSensorPosition(0);
  }

  public static double hoodPos() {
    return ShootingHood.getSelectedSensorPosition();
  }

  public static double shooterVelo() {
    return shooterr.getSelectedSensorVelocity();
  }

  public static void setShooter(double value) {
    shooterr.set(ControlMode.PercentOutput, value);
  }
}
