package frc.robot;

import java.lang.Math;

public class PID{
    double setpoint;
    double integral;
    double prev_error;
    double p,i,d, maxspeed;
    double merror;
    double piovertwo = 1.57079632679;
    double output;
    double minvalue = -1, maxvalue = 1;
    public PID(double mp, double mi, double md, double mmaxspeed){
        integral = 0;
        prev_error = 0;
        p = mp;
        i = mi;
        d = md;
        maxspeed = mmaxspeed;
        output = 0;
    }
    public void settarget(double m_setpoint){
        setpoint = m_setpoint;
    }
    public double pidcontrol(double target, double correction){  
        merror = target - correction;
        integral += (merror*.02);
        double derivative = (merror - prev_error) / 0.02;
        prev_error = merror;
        if(i == 0.0){
          output = ((p*merror) + (d*derivative));
        }
        else{
          output = ((p*merror) + (i*integral) + (d*derivative));
        }
        return (maxspeed/piovertwo)*(Math.atan(output));
      }
      public double pidcontrol(double correction){  
        double target = setpoint;
        merror = target - correction;
        integral += (merror*.02);
        double derivative = (merror - prev_error) / 0.02;
        prev_error = merror;
        output = ((p*merror) + (i*integral) + (d*derivative));
        output = (maxspeed/piovertwo)*(Math.atan(output));
        if (output > maxvalue){
          output = maxvalue;
      }
      if (output < minvalue){
          output = minvalue;
      }
        return output;

      }
      public void setkP(double kP){
        p = kP;
      }
      public void setkI(double kI){
        i = kI;
      }
      public void setkD(double kD){
        d = kD;
      }
      public double geterror(){
        return merror;
      }
      public double getoutput(){
        return output;
      }
      public double getPcomponent(){
        return (p*merror);
      }
      public void setminmax(double min, double max){
        minvalue = min;
        maxvalue = max;
      }
      public void setmax(double max){
        maxvalue = max;
      }

}




/**/ 