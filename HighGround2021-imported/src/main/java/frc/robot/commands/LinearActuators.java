/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

/*package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.ClimbBot;
import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.robot.Constants;
import edu.wpi.first.wpilibj.Joystick;


public class LinearActuators extends CommandBase {
  private final SendableChooser<String> m_chooser = new SendableChooser<>();
  private static final String kDefaultAuto = "Default";
  private static final String kCustomAuto = "My Auto";
  private final ClimbBot m_ClimbBot;
  double ClimbSpeed, climbinput, horizontalspeed, horizontalspeedsign, ClimbSpeed2;
  boolean reachedbottom1 , reachedbottom2, solstate=false, extend=false;
  Joystick controlstick = new Joystick(1);
  Joystick drivestick = new Joystick(0);
  int linExtended=2; 
  int tick=0;
  int autoaligningstage = 4;
  
  public LinearActuators(ClimbBot system) {
    m_ClimbBot = system;
    addRequirements(m_ClimbBot);
    m_chooser.setDefaultOption("Default Auto", kDefaultAuto);
    m_chooser.addOption("My Auto", kCustomAuto);
    SmartDashboard.putData("Auto choices", m_chooser);
  }
  
  @Override
  public void initialize() {
  }
  
  @Override
  public void execute() {
    //Switched between Forward and reverse using unused variables
    if(controlstick.getRawButtonPressed(Constants.linExtenderID)) {
      if(linExtended == 2) {
        linExtended = 3;
      }
      if(linExtended == 4) {
        linExtended = 1;
      }
    }

    //Changes between Forward and Reverse while storing an unused variable so it only runs once
    ClimbBot.setlinE(Value.kOff);
    if (linExtended == 1) {
      ClimbBot.setlinE(Value.kForward);
      linExtended = 2;
    } else if (linExtended == 3) {
      ClimbBot.setlinE(Value.kReverse);
      linExtended = 4;
    }

    //Horizontal Speed Logic
    horizontalspeedsign = horizontalspeed / Math.abs(horizontalspeed);
    
    if(linExtended == 3) {
      if(controlstick.getRawAxis(Constants.BarStrafeID) < 0.1 && controlstick.getRawAxis(Constants.BarStrafeID) > -0.1){
        if(horizontalspeed < 0.05 && horizontalspeed > -0.5){
          horizontalspeed = 0;
        } else {
          horizontalspeed -= 0.01 * horizontalspeedsign;
        }
      } else {
        horizontalspeed += controlstick.getRawAxis(Constants.BarStrafeID) / 50;
      }
      if (horizontalspeed > 1 || horizontalspeed < -1){
        horizontalspeed = horizontalspeedsign;
      }
    } else {
      horizontalspeed = 0;
    }

    if(controlstick.getRawButton(Constants.ExtenditID)) {
      autoaligningstage = 1;
      
    }
    else{
      if(autoaligningstage == 1){
        autoaligningstage = 2;
      }
      else if(autoaligningstage == 2 && ((ClimbBot.hes1v == true) && (ClimbBot.hes3v == true))){
        autoaligningstage = 3;
      }
      
    }
    //else 
        //true means not detected as digital imputs are inverse
        
        /*if(ClimbBot.hes1v==false /*|| reachedbottom1==true*///) {
        /*  reachedbottom1=true;
          if(ClimbBot.hes3v==false) {
            //ClimbSpeed = ClimbBot.getgyrox()*0.5;
          } else {
            //ClimbSpeed = (ClimbBot.getgyrox()*0.5)+1;
          }
        } else {
          ClimbSpeed = 1;
        }
        if(ClimbBot.hes3v==false /*|| reachedbottom2==true*///) {
        /*  reachedbottom2=true;
          if(ClimbBot.hes1v==false) {
            //ClimbSpeed2 = -ClimbBot.getgyrox()*0.4;
          } else {
            //ClimbSpeed2 = (-ClimbBot.getgyrox()*0.4)+1;
          }
        } else {
          ClimbSpeed2 = 1;
        }*/
    //}
    /*switch(autoaligningstage){
      case(1):
        ClimbSpeed = -1;
        ClimbSpeed2 = -1;
      break;
      case(2):
        ClimbSpeed = 1;
        ClimbSpeed2 = 1;
      break;
      case(3):
        if(((ClimbBot.hes1v == true) || (ClimbBot.hes3v == true))){
          ClimbSpeed = (ClimbBot.getgyrox()*0.5)+1;
          ClimbSpeed2 = (-ClimbBot.getgyrox()*0.4)+1;
        }
        else{
          ClimbSpeed = ClimbBot.getgyrox()*0.45;
          ClimbSpeed2 = -ClimbBot.getgyrox()*0.35;
        }
      break;

    }

    //Passes values to the ClimbBot subsystem
    if(/*linExtended == 1 ||*//*linExtended == 2){
      if(tick>100) {
        ClimbBot.setlin(ClimbSpeed, ClimbSpeed2);
        ClimbBot.setlinH(ClimbBot.getlindif()/100000);
      } else {
        ClimbBot.setlin(1, 1);
        ClimbBot.setlinH(0);
      }
      tick+=1;
    } else {
      tick=0;
      ClimbSpeed = 1;
      ClimbSpeed2 = 1;
      ClimbBot.setlin(1, 1);
      ClimbBot.setlinH(0);
    }
    
    //Changes SmartDashboard values based on variables
    SmartDashboard.putBoolean("Linear Actuators Extended?", solstate);
    if(ClimbBot.hes1v==false) {
      SmartDashboard.putBoolean("hes1", true);
      SmartDashboard.putString("lin1", "min height");
    } else {
      SmartDashboard.putBoolean("hes1", false);
    }
    if(ClimbBot.hes2v==false) {
      SmartDashboard.putBoolean("hes2", true);
      SmartDashboard.putString("lin1", "max height");
    } else {
      SmartDashboard.putBoolean("hes2", false);
    }
    if(ClimbBot.hes3v==false) {
      SmartDashboard.putBoolean("hes3", true);
      SmartDashboard.putString("lin2", "min height");
    } else {
      SmartDashboard.putBoolean("hes3", false);
    }
    if(ClimbBot.hes4v==false) {
      SmartDashboard.putBoolean("hes4", true);
      SmartDashboard.putString("lin2", "max height");
    } else {
      SmartDashboard.putBoolean("hes4", false);
    }

    SmartDashboard.putNumber("ClimbSpeed", -1*ClimbSpeed);
    SmartDashboard.putNumber("ClimbSpeed2", -1*ClimbSpeed2);
    SmartDashboard.putNumber("HorizontalSpeed", -1*horizontalspeed);
    SmartDashboard.putNumber("Gyro X", ClimbBot.getgyrox());
    SmartDashboard.putNumber("Lin Extended", linExtended);
    SmartDashboard.putNumber("Tick", tick);
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}*/
