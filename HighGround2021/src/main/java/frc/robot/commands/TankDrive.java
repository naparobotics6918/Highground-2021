/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.Drivetrain;
import edu.wpi.first.wpilibj.Joystick;
import frc.robot.Constants;
import frc.robot.Robot;

public class TankDrive extends CommandBase {
  private final Drivetrain m_Drivetrain;
  Joystick controlstick = new Joystick(1);
  Joystick drivestick = new Joystick(0);
  double rotation, vertical;

  public TankDrive(Drivetrain system) {
    m_Drivetrain = system;
    addRequirements(m_Drivetrain);
  }

  @Override
  public void initialize() {
    if(!Robot.autonomous){
      NetworkTableInstance.getDefault().getTable("limelight").getEntry("ledMode").setNumber(1);
    }
    
    SmartDashboard.putBoolean("Aimbot", false);
  }

  @Override
  public void execute() {
    vertical = drivestick.getRawAxis(Constants.verticalID)*-1;
    rotation = drivestick.getRawAxis(Constants.rotationID)*-1;
    Drivetrain.RKDrive(vertical, rotation, (0.5+((((drivestick.getRawAxis(Constants.speedID)*-1)+1)/2)*0.5)));
  }

  @Override
  public void end(boolean interrupted) {
  }

  @Override
  public boolean isFinished() {
    return false;
  }
}
