/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.ShooterControl;
import edu.wpi.first.wpilibj.Joystick;
import frc.robot.Constants;
import frc.robot.useful;
import frc.robot.Robot;
import java.lang.Math;
import frc.robot.PID;

public class Shooter extends CommandBase {
  private final ShooterControl m_shooterControl;
  public static double shootingvelocity;
  Joystick controlstick = new Joystick(1);
  Joystick drivestick = new Joystick(0);
  PID pid = new PID(0.4, 0.00000, 0.02, 0.4);
  PID flywheelcontrol = new PID(Constants.kP, Constants.kI, Constants.kD, 1.0);
  double adjustment, savedposition, before, after, getposition, target, tv, autovert, tanty, verticaljoystick, hoodpower, distance, position = 10, seconds = 0, ty = 8;
  boolean buttonpressed, shoot = false, revup = false, finished = false;
  double angleoffset;


  boolean intaking = false;

  public Shooter(ShooterControl system, boolean Revup) {
    m_shooterControl = system;
    addRequirements(m_shooterControl);
    revup = Revup;
  }

  @Override
  public void initialize() {
    finished = false;
    flywheelcontrol.settarget(30000);
    flywheelcontrol.setminmax(0.0, 1.0);
  }
  
  @Override
  public void execute() {
    distance = 1.5745 / tanty;
    if(Robot.autonomous){
      angleoffset = useful.minmaxnumber(((10.0/8.0) * (distance / 11.25)), 0.3, 1.0);
    }
    else{
      angleoffset = useful.minmaxnumber(((32.0/11.0) * (distance / 26.5)), 0.3, 0.71);
    }
    pid.setkP(0.5);
    hoodpower = (pid.pidcontrol((autovert), getposition));
    SmartDashboard.putNumber("Hood Output", (pid.getoutput()));
    SmartDashboard.putNumber("Hood Error", pid.geterror());
    SmartDashboard.putNumber("P component", pid.getPcomponent());
    shootingvelocity = ShooterControl.shooterVelo();
    if (shootingvelocity != 0) {
      SmartDashboard.putBoolean("Shooting Velocity", true);
    } else {
      SmartDashboard.putBoolean("Shooting Velocity", false);
    }

    //Turret Shooting Logic
    shoot = controlstick.getRawButton(Constants.ToggleShootID);

    if(Robot.autonomous) {
      shoot = true;
      if(revup) {
        if(shootingvelocity > 30000) {
          finished = true;
        }
      }
      else {
        seconds += 0.02;
        if(seconds >= 5){
          finished = true;
        }
      }
      SmartDashboard.putBoolean("Aimbot", true);
    }
    /*if(controlstick.getRawButtonPressed(Constants.ToggleShootID)) {
      shoot = !shoot;
    }*/
    if(shoot) {
      ShooterControl.setShooter(flywheelcontrol.pidcontrol(shootingvelocity));
    }
    else {
      ShooterControl.setShooter(0);
    }
    if(shootingvelocity < 28000){
      flywheelcontrol.setmax(1.0);
      flywheelcontrol.setkD(0);
    }
    else {
      flywheelcontrol.setmax(1);
      flywheelcontrol.setkD(Constants.kD);
    }
    //Turret Hood Logic
    ty = NetworkTableInstance.getDefault().getTable("limelight").getEntry("ty").getDouble(1);
    tv = NetworkTableInstance.getDefault().getTable("limelight").getEntry("tv").getDouble(0);
    getposition = 360.0*((ShooterControl.hoodPos()/ 9/*213.333333*/)/4098.0)*1.25;
    target = ((((-position)/1.25)/360.0)*4098.0)*213.333333;
    //Reset Shooting Hood to 0
    if(controlstick.getRawButtonReleased(Constants.ResetHoodID) && drivestick.getRawButtonReleased(Constants.ResetHoodID)) {
      ShooterControl.resetHood();
    }
    //Shooting Hood Output Math
    if(SmartDashboard.getBoolean("Aimbot", false) == true) {
      tanty = Math.tan(Math.toRadians(ty+22.0));
      autovert = (((180.0 / Math.PI) * (Math.atan((1.2865)/(distance))))) * angleoffset + 0;
      SmartDashboard.putNumber("atan check", Math.atan(0.1));
      if(tv == 1){
        
        if(autovert < 14.0 && autovert > 0) {
          ShooterControl.ShootingHood(hoodpower);
        }
        else if(autovert > 14.0) {
          ShooterControl.ShootingHood((pid.pidcontrol((13), getposition)));
        }
        else {
          ShooterControl.ShootingHood(0);
        }
      }
      else {
        ShooterControl.ShootingHood(0);
      }
      position = getposition;
      adjustment = 0;
      after = 0;
      before = 0;
    } 
    else {
      ShooterControl.ShootingHood(0);
    }
    
    //SmartDashboard Readings
    SmartDashboard.putNumber("Hood Angle", getposition);
    SmartDashboard.putNumber("Shooter Velocuty", shootingvelocity);
    SmartDashboard.putNumber("AutoVertOut", autovert);
    SmartDashboard.putNumber("Angle Offset", angleoffset);
    SmartDashboard.putNumber("ty", ty);
    SmartDashboard.putNumber("Distance", distance);
    //SmartDashboard.putBoolean("LimitSwitch", LimitSwitchTest.get());

    //SmartDashboard.putNumber("current draw", ShootingHood.getStatorCurrent());
    //ShootingHood.set(ControlMode.PercentOutput, hoodpower);
    
    //ShooterControl.ShootingHood(controlstick.getRawAxis(1)/8);


    //ShooterControl.setShooter(intaking ? 0.25 : 0);
  }
  
  @Override
  public void end(boolean interrupted) {
  }
  
  @Override
  public boolean isFinished() {
    return finished;
  }
}
