/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;


import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonFX;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import com.ctre.phoenix.motorcontrol.can.TalonFX;
import com.ctre.phoenix.motorcontrol.ControlMode;
import edu.wpi.first.wpilibj.Joystick;
import frc.robot.Constants;
import java.lang.Math;

public class Drivetrain extends SubsystemBase {
  static WPI_TalonFX r1 = new WPI_TalonFX(Constants.leftmotor1ID);
  static WPI_TalonFX r2 = new WPI_TalonFX(Constants.leftmotor2ID); 
  static WPI_TalonFX l1 = new WPI_TalonFX(Constants.rightmotor1ID); 
  static WPI_TalonFX l2 = new WPI_TalonFX(Constants.rightmotor2ID);
  static Joystick drivestick = new Joystick(0);
  public static double finalvertical=0, finalrotation=0, finalvertical2=0, finalvertical3=0, finalvertical4=0, finalvertical5=0, finalverticalold=0;
  static double leftmotorsinverted, rightmotorsinverted, inverted=1, speed;
  static double leftmotordistance = 0, rightmotordistance = 0;

  public Drivetrain() {
    r1.configFactoryDefault();
    r2.configFactoryDefault();
    l1.configFactoryDefault();
    l2.configFactoryDefault();
    rightmotorsinverted = 1;
    leftmotorsinverted = -1;
  }

  @Override
  public void periodic() {
    rightmotordistance = -r1.getSelectedSensorPosition();
    leftmotordistance = l1.getSelectedSensorPosition();
    //Inverts controls when button pressed
    if(SmartDashboard.getBoolean("Aimbot", false) == false) {
      if(inverted==1) {
        SmartDashboard.putString("Drivetrain Mode", "Intake Mode");
      } else {
         SmartDashboard.putString("Drivetrain Mode", "Shooter Mode");
      }
      if(drivestick.getRawButton(Constants.invertControlsID)) {
        //inverted = inverted * -1;
        inverted = -1;
      }
      else{
        inverted = 1;
      }
    } else {
      SmartDashboard.putString("Drivetrain Mode", "Assisted Mode"); 
    }
    //Resets drivetrain sensor positions when button pressed
    if(drivestick.getRawButtonPressed(Constants.resetTalonPosID)){
      for(int i = 0; i < 3; i++){
        new TalonFX(i).setSelectedSensorPosition(0);
      }
    }
  }

  public static void RKDrive(double vertical, double rotation, double k_speed) {
    double vertAccel = 0.015;
    double rotAccel = 0.1;
    speed = k_speed;
    finalverticalold=(finalvertical+finalvertical2+finalvertical3+finalvertical4+finalvertical5)/5;
    SmartDashboard.putNumber("finalverticalold", finalverticalold);
    SmartDashboard.putNumber("finalvertical", finalvertical);
    if(finalvertical > 0) { //Above Zero
      SmartDashboard.putString("FinalVertical", "Above Zero");
      if(finalverticalold - finalvertical > 0) {//if power increases
        SmartDashboard.putString("Drivetrain State", "Accelerating");
        //vertAccel = 0.03;
      } else if(finalverticalold - finalvertical < 0) {//if power decreases
        SmartDashboard.putString("Drivetrain State", "Decelerating");
        //vertAccel = 0.015;
      }
    } else if(finalvertical < 0) { //Below Zero
      SmartDashboard.putString("FinalVertical", "Below Zero");
      if(finalverticalold - finalvertical > 0) {  //if power increases
        SmartDashboard.putString("Drivetrain State", "Decelerating");
        //vertAccel = 0.015;
      } else if(finalverticalold - finalvertical < 0) {  //if power decreases
        SmartDashboard.putString("Drivetrain State", "Accelerating");
        //vertAccel = 0.03;
      }
    }
    //Modifies Vertical and Rotation so changes are gradual to make robot more stable
    if (vertical*inverted-vertAccel > finalvertical) {
      finalvertical = finalvertical+vertAccel;
    } else if(vertical*inverted+vertAccel < finalvertical) {
      finalvertical = finalvertical-vertAccel;
    } else {
      finalvertical = vertical*inverted;
    }
    if (rotation-rotAccel > finalrotation) {
      finalrotation = finalrotation+rotAccel;
    } else if(rotation+rotAccel < finalrotation) {
      finalrotation = finalrotation-rotAccel;
    } else {
      finalrotation = rotation;
    }

    //Final Math for drivetrain motors
    r1.set(ControlMode.PercentOutput, Math.pow((((vertical*inverted)-(rotation*0.75))*rightmotorsinverted)*speed, 3));
    SmartDashboard.putNumber("Final Right", Math.pow((((vertical*inverted)-(rotation*0.75))*rightmotorsinverted)*speed, 3));
    l1.set(ControlMode.PercentOutput, Math.pow((((vertical*inverted)+(rotation*0.75))*leftmotorsinverted)*speed, 3));
    SmartDashboard.putNumber("Final Left", Math.pow((((vertical*inverted)+(rotation*0.75))*leftmotorsinverted)*speed, 3));

    //Passes variables onto Smart Dashboard
    SmartDashboard.putNumber("Drivetrain Vertical", vertical*inverted);
    SmartDashboard.putNumber("Final Vertical", finalvertical);
    SmartDashboard.putNumber("Drivetrain Rotation", rotation*inverted);
    SmartDashboard.putNumber("Final Rotation", finalrotation);
    SmartDashboard.putNumber("Max Vert Accel", vertAccel);

    //Used to detect if speed is rising or falling
    finalvertical5=finalvertical4;
    finalvertical4=finalvertical3;
    finalvertical3=finalvertical2;
    finalvertical2=finalvertical;
  }

  public static double getdistanceave(){
    return (rightmotordistance + leftmotordistance) / 2;
  }

  public static double getdistancedifference(){
    return (rightmotordistance - leftmotordistance);
  }

  public static double getrightmotordistance(){
    return (rightmotordistance);
  }

  public static double getleftmotordistance(){
    return (leftmotordistance);
  }


  public static void resetdriveencoders(){
    r1.setSelectedSensorPosition(0);
    l1.setSelectedSensorPosition(0);
  }
}
