/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import com.ctre.phoenix.motorcontrol.can.VictorSPX;
import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import com.ctre.phoenix.motorcontrol.can.TalonFX;
import com.ctre.phoenix.motorcontrol.ControlMode;
import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.DigitalInput;
import com.kauailabs.navx.frc.AHRS;
import edu.wpi.first.wpilibj.SPI;
import frc.robot.Constants;

public class ClimbBot extends SubsystemBase {
  static DoubleSolenoid linE = new DoubleSolenoid(0, 1);
  static VictorSPX linH1 = new VictorSPX(Constants.linH1ID);
  static VictorSPX linH2 = new VictorSPX(Constants.linH2ID);
  static TalonFX lin1 = new TalonFX(Constants.lin1ID);
  static TalonFX lin2 = new TalonFX(Constants.lin2ID);
  DigitalInput hes1 = new DigitalInput(0);
  DigitalInput hes2 = new DigitalInput(1);
  DigitalInput hes3 = new DigitalInput(2);
  DigitalInput hes4 = new DigitalInput(3);
  static Joystick controlstick = new Joystick(1);
  private static boolean once1, once2;
  private static double yagment = 0;
  public static boolean hes1v;
  public static boolean hes2v;
  public static boolean hes3v;
  public static boolean hes4v;
  static AHRS gyro;

  public ClimbBot() {
    gyro = new AHRS(SPI.Port.kMXP);
    lin1.setInverted(false);
    lin2.setInverted(false);
  }

  @Override
  public void periodic() {
    //Grabs the Hall Effect Sensor values from the Climb Subsystem
    hes1v=hes1.get();
    hes2v=hes2.get();
    hes3v=hes3.get();
    hes4v=hes4.get();

    //Passes Variables to SmartDashboard
    SmartDashboard.putNumber("lin1pos", lin1.getSelectedSensorPosition());
    SmartDashboard.putNumber("lin2pos", lin2.getSelectedSensorPosition());
    SmartDashboard.putNumber("lindif", getlindif());

    SmartDashboard.putNumber("gyro.getPitch();", gyro.getPitch());
    SmartDashboard.putNumber("gyro.getYaw();", gyro.getYaw());
    SmartDashboard.putNumber("gyro.getRoll();", gyro.getRoll());

    //The first time the linear actuators are in a retracted state the motors sensor position is reset to 0
    if(!hes1v) {
      if(!once1) {
        once1 = true;
        lin1.setSelectedSensorPosition(0);
      }
    } else {
      once1 = false;
    }
    if(!hes3v) {
      if(!once2) {
        once2 = true;
        lin2.setSelectedSensorPosition(0);
      }
    } else {
      once2 = false;
    }
  }

  public static void setlin(double ClimbSpeed, double ClimbSpeed2) {
    //Limits ClimbSpeeds to a max of 0.3
    if(ClimbSpeed>0.3) {
      ClimbSpeed = 0.3;
    } else if(ClimbSpeed<-0.3) {
      ClimbSpeed = -0.3;
    }
    if(ClimbSpeed2>0.3) {
      ClimbSpeed2 = 0.3;
    } else if(ClimbSpeed2<-0.3) {
      ClimbSpeed2 = -0.3;
    }

    //Prevents Linear Actuators from going past maximums
    if(((ClimbSpeed > 0 && ClimbBot.hes1v == false)) || (ClimbSpeed < 0 && ClimbBot.hes2v == false)) {
      ClimbSpeed=0;
      SmartDashboard.putBoolean("errorcheck", true);
    } else {
      
      SmartDashboard.putBoolean("errorcheck", false);
    }
    if((ClimbSpeed2 > 0 && ClimbBot.hes3v == false) || (ClimbSpeed2 < 0 && ClimbBot.hes4v == false)) {
      ClimbSpeed2=0;
      SmartDashboard.putBoolean("errorcheck2", true);
    } else {
      SmartDashboard.putBoolean("errorcheck2", false);
    }
    
    SmartDashboard.putNumber("finalclimbspeed1", -ClimbSpeed*0.65);
    SmartDashboard.putNumber("finalclimbspeed2", -ClimbSpeed2*0.65);
    lin1.set(ControlMode.PercentOutput, -ClimbSpeed*0.65);
    lin2.set(ControlMode.PercentOutput, -ClimbSpeed2*0.65);
    
    
  }
  
  public static void setlinH(double horizontalspeed) {
    linH1.set(ControlMode.PercentOutput, horizontalspeed);
    linH2.set(ControlMode.PercentOutput, horizontalspeed);
  }

  public static void setlinE(Value value) {
    linE.set(value);
  }

  public static double getlindif() {
    return lin1.getSelectedSensorPosition()-lin2.getSelectedSensorPosition();
  }

  public static double getgyrox() {
    return (gyro.getYaw() - yagment); 
  }

  public static void zeropitch() {
    yagment = gyro.getYaw();
  }
}
