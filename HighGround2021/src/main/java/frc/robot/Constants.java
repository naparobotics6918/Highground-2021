/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

/**
 * The Constants class provides a convenient place for teams to hold robot-wide numerical or boolean
 * constants.  This class should not be used for any other purpose.  All constants should be
 * declared globally (i.e. public static).  Do not put anything functional in this class.
 *
 * <p>It is advised to statically import this class (or one of its inner classes) wherever the
 * constants are needed, to reduce verbosity.
 */
public final class Constants {
	
//Encoder Values

	//Drivetrain system
    public static final int leftmotor1ID = 0;
    public static final int leftmotor2ID = 1;
    public static final int rightmotor1ID = 2;
	public static final int rightmotor2ID = 3;

	//Intake system
	public static final int intakeHorizontalID = 22; //no motor
	public static final int intakeVerticalID = 5;
	public static final int coveyerID = 6;

	//Climb system
    public static final int lin1ID = 11;
    public static final int lin2ID = 12;
	public static final int linH1ID = 10;
	public static final int linH2ID = 7;

	//Shooter system
	public static final int shooterlID = 9;
	public static final int shooterrID = 4;
	public static final int ShootingHoodID = 8;

	//Control Panel system
	//public static final int spinnerID = 13; 

//Controller Mappings

	//Drivestick Axes
    public static final int verticalID = 1;
	public static final int rotationID = 2;
	public static final int speedID = 3;

	//Drivestick Buttons
	public static final int AutoToggleID = 1;
	public static final int resetTalonPosID = 9;
	public static final int invertControlsID = 2;


	//Controlstick Axes
	public static final int BarStrafeID = 0;

	//Controlstick Buttons
	public static final int ToggleIntakeID = 2;
	public static final int ToggleShootID = 1;
	public static final int ConveyerDownID = 3;
	public static final int ConveyerUpID = 5;
	public static final int ResetHoodID = 7;
	public static final int IntakeExtenderID = 10;
	public static final int ExtenditID = 11;
	public static final int linExtenderID = 12;


    /**
	 * Which PID slot to pull gains from. Starting 2018, you can choose from
	 * 0,1,2 or 3. Only the first two (0,1) are visible in web-based
	 * configuration.
	 */
	public static final int kSlotIdx = 0;

	/**
	 * Talon SRX/ Victor SPX will supported multiple (cascaded) PID loops. For
	 * now we just want the primary one.
	 */
	public static final int kPIDLoopIdx = 0;

	/**
	 * Set to zero to skip waiting for confirmation, set to nonzero to wait and
	 * report to DS if action fails.
	 */
    public static final int kTimeoutMs = 30;

	/**
	 * PID Gains may have to be adjusted based on the responsiveness of control loop.
     * kF: 1023 represents output value to Talon at 100%, 7200 represents Velocity units at 100% output
     */

    public final static double kP = 0.175;
	public final static double kI = 0.0;
	public final static double kD = 0.01;
	public final static double kF = 1023.0/7200.0;
	public final static int kIzone = 300;
	public final static double kPeakOutput = 1.00;
	public static final double Pi = 3.1415926535897932384626433832795028841971693993751058209749445923078164062862089986280348253421170679821480865132823066470938446095505822317253594081284811174502841027019385211055596446229489549303819644288109756659334461284756482337867831652712019091456485669234603486104543266482133936072602491412737245870066063155881748815209209628292540917153643678925903600113305305488204665213841469519415116094330572703657595919530921861173819326117931051185480744623799627495673518857527248912279381830119491298336733624406566430860213949463952247371907021798609437027705392171762931767523846748184676694051320005681271452635608277857713427577896091736371787214684409012249534301465495853710507922796892589235420199561121290219608640344181598136297747713099605187072113499999983729780499510597317328160963185950244594553469083026425223082533446850352619311881710100031378387528865875332083814206171776691473035982534904287554687311595628638823537875937519577818577805321712268066130019278766111959092164201989;
	
	public class autoconstants{
		public static final double drivedistance = -18.0;
		public static final double drivespeed = 0.5;

	}


}
