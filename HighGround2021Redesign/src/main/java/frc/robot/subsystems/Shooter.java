// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.TalonFXFeedbackDevice;
import com.ctre.phoenix.motorcontrol.TalonFXInvertType;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonFX;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;

public class Shooter extends SubsystemBase {
  WPI_TalonFX flywheelLeft = new WPI_TalonFX(Constants.shooterlID);
  WPI_TalonFX flywheelRight = new WPI_TalonFX(Constants.shooterrID);
  /** Creates a new Shooter. */
  public Shooter() {
     /* Factory Default all hardware to prevent unexpected behaviour */
		flywheelLeft.configFactoryDefault();
		
		/* Config neutral deadband to be the smallest possible */
		flywheelLeft.configNeutralDeadband(0.001);

		/* Config sensor used for Primary PID [Velocity] */
        flywheelLeft.configSelectedFeedbackSensor(TalonFXFeedbackDevice.IntegratedSensor,
                                            Constants.velocitycontrol.kPIDLoopIdx, 
											Constants.velocitycontrol.kTimeoutMs);
											

		/* Config the peak and nominal outputs */
		flywheelLeft.configNominalOutputForward(0.2, Constants.velocitycontrol.kTimeoutMs);
		flywheelLeft.configNominalOutputReverse(0, Constants.velocitycontrol.kTimeoutMs);
		flywheelLeft.configPeakOutputForward(1, Constants.velocitycontrol.kTimeoutMs);
		flywheelLeft.configPeakOutputReverse(0, Constants.velocitycontrol.kTimeoutMs);

		/* Config the Velocity closed loop gains in slot0 */
		flywheelLeft.config_kF(Constants.velocitycontrol.kPIDLoopIdx, Constants.velocitycontrol.kGains_Velocit.kF, Constants.velocitycontrol.kTimeoutMs);
		flywheelLeft.config_kP(Constants.velocitycontrol.kPIDLoopIdx, Constants.velocitycontrol.kGains_Velocit.kP, Constants.velocitycontrol.kTimeoutMs);
		flywheelLeft.config_kI(Constants.velocitycontrol.kPIDLoopIdx, Constants.velocitycontrol.kGains_Velocit.kI, Constants.velocitycontrol.kTimeoutMs);
    flywheelLeft.config_kD(Constants.velocitycontrol.kPIDLoopIdx, Constants.velocitycontrol.kGains_Velocit.kD, Constants.velocitycontrol.kTimeoutMs);
    flywheelLeft.configClosedloopRamp(2, Constants.velocitycontrol.kTimeoutMs);
    /*
		 * Talon FX does not need sensor phase set for its integrated sensor
		 * This is because it will always be correct if the selected feedback device is integrated sensor (default value)
		 * and the user calls getSelectedSensor* to get the sensor's position/velocity.
		 * 
		 * https://phoenix-documentation.readthedocs.io/en/latest/ch14_MCSensor.html#sensor-phase
		 */

    flywheelLeft.setInverted(TalonFXInvertType.CounterClockwise);
    flywheelRight.follow(flywheelLeft);
    flywheelRight.setInverted(TalonFXInvertType.OpposeMaster);
  }

  @Override
  public void periodic() {    
    SmartDashboard.putNumber("RPM", flywheelLeft.getSelectedSensorVelocity() * (600.0/2048.0));
  }
  public void setRPM(double demand){
    double units = demand * (2048.0/600.0);
    flywheelLeft.set(ControlMode.Velocity, units);
  }
  public void stop(){
    flywheelLeft.stopMotor();
  }
}
