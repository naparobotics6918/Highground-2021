/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.Drivetrain;
import edu.wpi.first.wpilibj.Joystick;
import frc.robot.Constants;
import frc.robot.Robot;
import frc.robot.PID;
import java.lang.Math;

public class AutoPilot extends CommandBase {
  private final Drivetrain m_Drivetrain;
  Joystick drivestick = new Joystick(0);
  Joystick controlstick = new Joystick(1);
  PID pid = new PID(0.78, 0.4, 0.22, 0.65);
  double tx, targetArea, tv, rotation, vertical, P, I, D;
  boolean finished = false;

  public AutoPilot(Drivetrain system) {
    m_Drivetrain = system;
    addRequirements(m_Drivetrain);
  }

  @Override
  public void initialize() {
    NetworkTableInstance.getDefault().getTable("limelight").getEntry("ledMode").setNumber(0);
    SmartDashboard.putBoolean("Aimbot", true);
    pid.settarget(0);
    finished = false;
  }

  @Override
  public void execute() {
    tx = NetworkTableInstance.getDefault().getTable("limelight").getEntry("tx").getDouble(0) - 0.20;
    tv = NetworkTableInstance.getDefault().getTable("limelight").getEntry("tv").getDouble(0);
    SmartDashboard.putNumber("Target Horizontal", tx);

    //Logic for turning off autonomous
    if(drivestick.getRawButtonReleased(Constants.AutoToggleID)) {
      finished = true; 
    } else {
      finished = false; 
    }

    //Logic for Drivetrain Aimbot
    vertical = drivestick.getRawAxis(Constants.verticalID)*-1;
    rotation = pid.pidcontrol(tx)*1;

    //Prevents rotation if target not detected
    if(tv == 0) {
      SmartDashboard.putBoolean("Target Detected?", false);
      rotation = 0;
    } else {
      SmartDashboard.putBoolean("Target Detected?", true);
    }

    //Prevents motors from stalling
    if((rotation + vertical) < 0.4 && (rotation + vertical) > - 0.4){
      rotation = 0;
    }

    //Sends Calculated Variables to Drivetrain
    Drivetrain.RKDrive(vertical, rotation, 1.0);

    //Moves onto next stage in Autonomous if these conditions are true
    if(Robot.autonomous && Math.abs(tx) < 1.0 && tv == 1){
      finished = true;
    }
  }

  @Override
  public void end(boolean interrupted) {
    SmartDashboard.putBoolean("Target Detected?", false);
    SmartDashboard.putNumber("Target Horizontal", 0);
    SmartDashboard.putBoolean("Aimbot", false);
    Drivetrain.RKDrive(0, 0, 1);
  }

  @Override
  public boolean isFinished() {
    return finished;
  }
}