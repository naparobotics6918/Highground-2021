// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.PID;
import frc.robot.subsystems.Drivetrain;

public class AutoAlign extends CommandBase {
  private final Drivetrain subsystem;
  double tx, rotation;
  PID pid = new PID(0.055, 0.0007, 0.007, 0.65);
  /** Creates a new AutoAlign. */
  public AutoAlign(Drivetrain decSubsystem) {
    subsystem = decSubsystem;
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(decSubsystem);
  }

  // Called when the command is initially scheduled.
  @Override
    public void initialize() {
      NetworkTableInstance.getDefault().getTable("limelight").getEntry("ledMode").setNumber(0);
      pid.settarget(0);
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {

    //i made this line before i knew what feed forward was i should remove it from this line and incorperate it into pid control when i have time
    tx = NetworkTableInstance.getDefault().getTable("limelight").getEntry("tx").getDouble(0) - 2.5;
    rotation = pid.pidcontrol(tx)*1;
    subsystem.setMotors(rotation, -rotation);
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {}

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
